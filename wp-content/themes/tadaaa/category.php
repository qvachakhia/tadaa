<?php get_header(); ?>
<div class="fullwith-original-cat">
  <div class="container first-section">
    <div class="row">
        <?php if(have_posts()) : ?>
        <?php while(have_posts()) : the_post(); ?>
          <?php set_query_var('category','cat'); ?>
          <?php get_template_part('template/card') ?>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <?php if(function_exists('wp_paginate')) {wp_paginate();} ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
