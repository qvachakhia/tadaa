<?php get_header(); ?>
<div class="fullwith-original-cat">
  <div class="container first-section">
    <div class="row">
        <?php if(have_posts()) : ?>
        <?php while(have_posts()) : the_post(); ?>
          <div class="col-lg-3">
            <div class="ccard">
              <p><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post'); ?></a></p>
              <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
              <div class="ccinfo">
                <p class="ccdate"><?php echo my_custom_date( get_the_date() ); ?></p>
              </div>
            </div>
          </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <?php if(function_exists('wp_paginate')) {wp_paginate();} ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
