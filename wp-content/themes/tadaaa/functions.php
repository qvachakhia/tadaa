<?php
//THMB REGISTRATION
add_theme_support('post-thumbnails');
add_image_size('post', 640, 360, true);
add_image_size('main-post-large', 1280, 720, true);

//YOUTUBE RESPONSIVE
add_filter( 'embed_oembed_html', 'wrap_embed_with_div', 10, 3 );
function wrap_embed_with_div( $html ) {
    return '<div class="hbps-responsive-video">' . $html . '</div>';
}


//REMOVE P
remove_filter ('the_exceprt', 'wpautop');

//SIDEBAR REGISTRATION
if(function_exists('register_sidebar'))
    register_sidebar(array('name' => 'Sidebar 1',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<p class="vidget_title">',
		'after_title' => '</p>',
		));
add_filter('wp_title', create_function('$a, $b','return str_replace(" $b ","",$a);'), 10, 2);
if(function_exists('register_sidebar'))
    register_sidebar(array('name' => 'Sidebar 2',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<p class="vidget_title">',
		'after_title' => '</p>',
		));

//MENU REGISTRATION
if ( function_exists( 'register_nav_menus' ) )
  {
    register_nav_menus (
      array (
        'header-menu' => 'Header Menu',
				'header-menu' => 'Category'
        )
    );
  }

//REMOVE REL ATRIBUTE FROM THE CATEGORY
function remove_category_list_rel($output)
{
  $output = str_replace(' rel="category tag"', '', $output);
  return $output;
}
add_filter('wp_list_categories', 'remove_category_list_rel');
add_filter('the_category', 'remove_category_list_rel');


//REMOVR WP STYLE
add_action( 'wp_print_styles', 'my_deregister_styles', 100 );
function my_deregister_styles() {
	wp_deregister_style( 'contact-form-7' );
	wp_deregister_style( 'simple-pagination-css' );
  //deregister as many stylesheets as you need...
}

// This goes in function.php
class My_Walker extends Walker_Nav_Menu
{
	// function start_el(&$output, $item, $depth, $args) {
	// 	global $wp_query;
	// 	$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

	// 	$class_names = $value = '';

	// 	$classes = empty( $item->classes ) ? array() : (array) $item->classes;

	// 	$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
	// 	$class_names = '';

	// 	$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

	// 	$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
	// 	$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
	// 	$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
	// 	$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

	// 	$item_output = $args->before;
	// 	$item_output .= '<a'. $attributes .'>';
	// 	$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
	// 	$item_output .= '<span>' . $item->description . '</span>';
	// 	$item_output .= '</a>';
	// 	$item_output .= $args->after;

	// 	$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	// }
}

function wp_get_attachment( $attachment_id ) {

	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}

if(extension_loaded("zlib") && (ini_get("output_handler") != "ob_gzhandler"))
   add_action('wp', create_function('', '@ob_end_clean();@ini_set("zlib.output_compression", 1);'));

function ajx_sharpen_resized_files( $resized_file ) {
    $image = wp_load_image( $resized_file );
    if ( !is_resource( $image ) )
        return new WP_Error( 'error_loading_image', $image, $file );
    $size = @getimagesize( $resized_file );
    if ( !$size )
        return new WP_Error('invalid_image', __('Could not read image size'), $file);
    list($orig_w, $orig_h, $orig_type) = $size;
    switch ( $orig_type ) {
        case IMAGETYPE_JPEG:
            $matrix = array(
                array(-1, -1, -1),
                array(-1, 16, -1),
                array(-1, -1, -1),
            );
            $divisor = array_sum(array_map('array_sum', $matrix));
            $offset = 0;
            imageconvolution($image, $matrix, $divisor, $offset);
            imagejpeg($image, $resized_file,apply_filters( 'jpeg_quality', 90, 'edit_image' ));
            break;
        case IMAGETYPE_PNG:
            return $resized_file;
        case IMAGETYPE_GIF:
            return $resized_file;
    }
    return $resized_file;
}
add_filter('image_make_intermediate_size', 'ajx_sharpen_resized_files',900);

//SINGLE CUSTOM TEMPLATE REGISTRATION
function get_custom_cat_template($single_template) {
	global $post;

	if ( in_category( 'masterklasebi' )) {
	  $single_template = dirname( __FILE__ ) . '/test1.php';
	}

  if ( in_category( 'kursebi' )) {
	  $single_template = dirname( __FILE__ ) . '/test2.php';
	}

	return $single_template;
}
add_filter( "single_template", "get_custom_cat_template" ) ;

// SOCIAL MEDIA SHARE BUTTONS
function wcr_share_buttons() {
    $url = urlencode(get_the_permalink());
    $title = urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'));
    $media = urlencode(get_the_post_thumbnail_url(get_the_ID(), 'full'));

    include( locate_template('share-template.php', false, false) );
}

//GEORGIAN DATE
function my_custom_date($date){
    $new_date = getdate(strtotime($date));
    $my_months=[
            'January'   => 'იან.',
            'February'  => 'თებ.',
            'March'     => 'მარ.',
            'April'     => 'აპრ.',
            'May'       => 'მაი.',
            'June'      => 'ივნ.',
            'July'      => 'ივლ.',
            'August'    => 'აგვ.',
            'September' => 'სექ.',
            'October'   => 'ოქტ.',
            'November'  => 'ნოე.',
            'December'  => 'დეკ.',
        ];
        $new_date['month']= $my_months[$new_date['month']];
        return $new_date['mday']. ' '. $new_date['month']. ' ' .$new_date['year'];
    }


//REMOVE UNNECESSARY FUNCTIONS
function nowp_head_cleanup() {
    // Remove junk from head
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'feed_links_extra', 3);

    global $wp_widget_factory;
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));

    if (!class_exists('WPSEO_Frontend')) {
        remove_action('wp_head', 'rel_canonical');
        add_action('wp_head', 'nowp_rel_canonical');
    }
}
function nowp_rel_canonical() {
    global $wp_the_query;

    if (!is_singular()) {
        return;
    }

    if (!$id = $wp_the_query->get_queried_object_id()) {
        return;
    }

    $link = get_permalink($id);
    echo "\t<link rel=\"canonical\" href=\"$link\">\n";
}
add_action('init', 'nowp_head_cleanup');

//Disable gutenberg style in Front
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

function remove_json_api () {

    // Remove the REST API lines from the HTML Header
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

    // Remove the REST API endpoint.
    remove_action( 'rest_api_init', 'wp_oembed_register_route' );

    // Turn off oEmbed auto discovery.
    add_filter( 'embed_oembed_discover', '__return_false' );

    // Don't filter oEmbed results.
    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

    // Remove oEmbed discovery links.
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );

   // Remove all embeds rewrite rules.
   add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );

}
add_action( 'after_setup_theme', 'remove_json_api' );

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

add_action( 'init', 'remove_dns_prefetch' );
function  remove_dns_prefetch () {
   remove_action( 'wp_head', 'wp_resource_hints', 2, 99 );
}

/* Remove the WordPress version*/
add_filter('the_generator', '__return_false');


//GET PREV NEXT
function getPrevNext(){
	$pagelist = get_pages('sort_column=menu_order&sort_order=asc');
	$pages = array();
	foreach ($pagelist as $page) {
	   $pages[] += $page->ID;
	}

	$current = array_search(get_the_ID(), $pages);
	$prevID = $pages[$current-1];
	$nextID = $pages[$current+1];

	echo '<div class="getprevnext">';

	if (!empty($prevID)) {
		echo '<div class="alignleft">';
		echo '<a href="';
		echo get_permalink($prevID);
		echo '"';
		echo 'title="';
		echo get_the_title($prevID);
		echo'">Previous</a>';
		echo "</div>";
	}
	if (!empty($nextID)) {
		echo '<div class="alignright">';
		echo '<a href="';
		echo get_permalink($nextID);
		echo '"';
		echo 'title="';
		echo get_the_title($nextID);
		echo'">Next</a>';
		echo "</div>";
	}
}

// COUNT POST VIEWS
function gt_get_post_view() {
    $count = get_post_meta( get_the_ID(), 'post_views_count', true );
    return "$count views";
}
function gt_set_post_view() {
    $key = 'post_views_count';
    $post_id = get_the_ID();
    $count = (int) get_post_meta( $post_id, $key, true );
    $count++;
    update_post_meta( $post_id, $key, $count );
}
function gt_posts_column_views( $columns ) {
    $columns['post_views'] = 'Views';
    return $columns;
}
function gt_posts_custom_column_views( $column ) {
    if ( $column === 'post_views') {
        echo gt_get_post_view();
    }
}
add_filter( 'manage_posts_columns', 'gt_posts_column_views' );
add_action( 'manage_posts_custom_column', 'gt_posts_custom_column_views' );


//post_views
function setPostViews($postID) {
    $countKey = 'post_views_count';
    $count = get_post_meta($postID, $countKey, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $countKey);
        add_post_meta($postID, $countKey, '0');
    }else{
        $count++;
        update_post_meta($postID, $countKey, $count);
    }

}

function count_cat_post($category) {
if(is_string($category)) {
    $catID = get_cat_ID($category);
}
elseif(is_numeric($category)) {
    $catID = $category;
} else {
    return 0;
}
$cat = get_category($catID);
return $cat->count;
}

function special_nav_class($classes, $item){
    if( in_array('current-menu-item', $classes) ){
        $classes[] = 'active';
    }
    return $classes;
}
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

?>
