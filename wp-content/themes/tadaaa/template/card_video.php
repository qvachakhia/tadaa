<?php if ($iteration == 1): ?>
	<div class="card video-box">
	   	<img class="card-img-top popular-image" src="<?php echo get_the_post_thumbnail_url()  ?>" alt="tadaa.ge">
	    
	    <div class="card-body">
	    	<a href="<?php the_permalink(); ?>" class="popular-box-title"><?php the_title(); ?></a>
	    </div>
	</div>
<?php endif ?>

<?php if ($iteration >= 2): ?>

	<div class="left-video-box">
	  <div class="lefttbox-card">
	    <div class="lefttbox-readtime">
	    	<div class="row">
          <div class="col-12 col-md-2">
            <figure class="post-gallery">
              <a href="<?php the_permalink(); ?>">
              <img width="270" height="270" src="<?php echo get_the_post_thumbnail_url()  ?>" class="video-news-image" alt="" sizes="74px" data-src="" data-sizes="auto" data-srcset="">
              </a>
            </figure>
          </div>
          <div class="col-6 col-md-10">
            <div class="video-block-meta">
              <span class="lefttbox-date"><?php echo my_custom_date( get_the_date() ); ?></span>
              <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
            </div>
          </div>
	      </div>
	    </div>
	  </div>
	</div>
<?php endif ?>


<!-- ვიდეოს ბლოკი -->
<div class="fullwith-original">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <p class="title-p">
          <span class="r-title">ვიდეოები</span>
          <span class="cat-lines"></span>
        </p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">

      <?php
        $total_post = count_cat_post('ვიდეო');
      ?>  
      <?php query_posts('category_name=video&orderby=desc&showposts=5'); ?>
      <?php $iteration = 0; ?>

        <div class="col-sm-8">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); $iteration++ ?>
                <?php if ($iteration == 1): ?>
                    <?php include( locate_template( 'template/card_video.php', false, false ) );  ?>
                <?php endif; ?>
            <?php endwhile; ?>
            <?php endif; ?>

        </div>

        <div class="col-sm-4">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
                <?php if ($wp_query->current_post - 0): ?>
                    <?php include( locate_template( 'template/card_video.php', false, false ) );  ?>
                <?php endif; ?>

            <?php endwhile; ?>
            <?php endif; ?>
            <?php if ($total_post > 3): ?>
                <div class="mx-auto see-video"><a class="read-more-button" href="/?cat=264" target="_self" role="button">
                <span>იხილეთ მეტი</span></a></div>
            <?php endif; ?>
        </div>

      
    </div>
  </div>
</div>
