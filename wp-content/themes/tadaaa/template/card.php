<div class="col-lg-4">
    <?php if ($category): ?>
      <div class="tcard" id='category-card'>
    <?php else: ?>
      <div class="tcard">
    <?php endif ?>
    <p><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post'); ?></a></p>
      <p class="ccat"><?php the_category(', '); ?> |  <span class="cdate"><?php echo my_custom_date( get_the_date() ); ?></span></p>

    <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
    <?php if ($category): ?>
    <?php else: ?>
      <div class="cinfo">
        <?php echo wp_trim_words( get_the_content(), 15, '...' );?>
      </div>
    <?php endif ?>
    
  </div>
  <?php if ($category): ?>
  	<br>
 	<br>
  <?php endif ?>
  	
</div>
