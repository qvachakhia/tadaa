<?php get_header(); ?>
<div class="container">
  <div class="row">
      <?php if(have_posts()) : ?>
      <?php while(have_posts()) : the_post(); ?>
            <div class="col-3">
              <div class="tcard">
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post'); ?></a>
                <span class="scat"><?php the_category(', '); ?></span>
                <span class="sdate">| <?php echo my_custom_date( get_the_date() ); ?></span>
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
              </div>
            </div>
      <?php endwhile; ?>
      <?php endif; ?>
  </div>
</div>
<?php get_footer(); ?>
