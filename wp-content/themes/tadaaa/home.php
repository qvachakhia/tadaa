<?php /* Template Name: home */ ?>

<?php get_header('home'); ?>

<div class="fullwith-slider">
  <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <p class="title-p">
            <span class="r-title">ტოპ სიახლე</span>
          </p>
        </div>
        <div class="col-sm-4 konstributor">
          <p class="title-p">
            <span class="r-title">სიახლეები</span>
          </p>
        </div>
        <div class="col-sm-8">
          

          <?php
            $posts = wmp_get_popular( array( 'limit' => 1, 'post_type' => 'post', 'range' => 'daily' ) );
            global $post;
            if ( count( $posts ) > 0 ): foreach ( $posts as $post ):
          ?>
            <a href="<?php the_permalink(); ?>">
               <div class="card popular-box">
                  <img class="card-img-top popular-image" src="<?php echo get_the_post_thumbnail_url()  ?>" alt="tadaa.ge">
                  <div class="card-text">
                    <a href="<?php the_permalink(); ?>" class="popular-box-title"><?php the_title(); ?></a>
                  </div>
                  <div class="cinfo" id="slider_desc">
                    <?php 
                      $string = strip_tags($post->post_content);
                      echo $string;
                    ?>
                  </div>
                </div>
            </a>
          <?php
            endforeach; endif;
          ?>
        </div>
       
        <div class="col-sm-4">

          <div class="lefttbox">
          
            <?php query_posts('category_name=news&orderby=desc&showposts=5'); ?>
           
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
               
              <div class="lefttbox-card">
                <div class="lefttbox-readtime">
                  <div class="row">
                    <div class="col-12 col-md-2">
                      <figure class="post-gallery">
                        <a href="<?php the_permalink(); ?>">

                        <img width="270" height="270" src="<?php echo get_the_post_thumbnail_url() ?>" class="last-news-image" alt="" sizes="74px" data-src="" data-sizes="auto" data-srcset="">
                        </a>
                      </figure>
                    </div>
                    <div class="col-6 col-md-10">
                      <div class="last-news-meta">
                        <span class="lefttbox-date"><?php echo my_custom_date( get_the_date() ); ?></span>
                        
                        <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
  </div>
</div>
<!--<div class="fullwith-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        test
      </div>
    </div>
  </div>
</div>-->
<div class="fullwith-original news-desctop">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <p class="title-p">
          <span class="r-title">სიახლეები</span>
          
        </p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <?php
        $total_post = count_cat_post('სიახლეები');
      ?>  
      <?php query_posts('category_name=news&orderby=desc&showposts=3'); ?>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part('template/card') ?>
      <?php endwhile; ?>

      <?php if ($total_post > 3): ?>
        <div class="mx-auto read-more-area"><a class="read-more-button" href="/?cat=20" target="_self" role="button">
        <span>იხილეთ მეტი</span></a></div>
      <?php endif ?>

      <?php endif; ?>
    </div>
  </div>
</div>


<div class="fullwith-original">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <p class="title-p">
          <span class="r-title">ბიზნესი</span>
          
        </p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <?php
        $total_post = count_cat_post('ბიზნესი');
      ?>  
      <?php query_posts('category_name=business&orderby=desc&showposts=3'); ?>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
       <?php get_template_part('template/card') ?>
      <?php endwhile; ?>

      <?php if ($total_post > 3): ?>
        <div class="mx-auto read-more-area"><a class="read-more-button" href="/?cat=1" target="_self" role="button">
        <span>იხილეთ მეტი</span></a></div>
      <?php endif ?>
      
      <?php endif; ?>
    </div>
  </div>
</div>


<div class="fullwith-original">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <p class="title-p">
          <span class="r-title">კრეატივი</span>
          
        </p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <?php
        $total_post = count_cat_post('კრეატივი');
      ?> 
      <?php query_posts('category_name=creative&orderby=desc&showposts=3'); ?>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part('template/card') ?>
      <?php endwhile; ?>

      <?php if ($total_post > 3): ?>
        <div class="mx-auto read-more-area"><a class="read-more-button" href="/?cat=18" target="_self" role="button">
        <span>იხილეთ მეტი</span></a></div>
      <?php endif ?>

      <?php endif; ?>
    </div>
  </div>
</div>

<div class="fullwith-original">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <p class="title-p">
          <span class="r-title">ტექნოლოგიები</span>
           
        </p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <?php
        $total_post = count_cat_post('ტექნოლოგიები');
      ?> 
      <?php query_posts('category_name=technology&orderby=desc&showposts=3'); ?>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part('template/card') ?>
      <?php endwhile; ?>

      <?php if ($total_post > 3): ?>
        <div class="mx-auto read-more-area"><a class="read-more-button" href="/?cat=21" target="_self" role="button">
        <span>იხილეთ მეტი</span></a></div>
      <?php endif ?>

      <?php endif; ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
