<?php get_header('post'); ?>
<div class="container first-section">
  <div class="row">
    <div class="col-1">
    </div>
    <div class="col-lg-10 col-sm-12">
      <div class="singlewrap">
       
        <?php if(have_posts()) : ?>
        <?php while(have_posts()) : the_post(); ?>
          <h1 class="stitle"><?php the_title(); ?></h1>
          <div class="sinfo">
            <span class="scat"><?php the_category(', '); ?></span>
            <span class="sdate">| <?php echo my_custom_date( get_the_date() ); ?><!-- | </span><span class="sreadtime"><?php echo $cfs->get('read_time'); ?> წუთში წასაკითხი</span>-->
          </div>
          
          <!--
          <div class="sads">
            <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/imgs/logo.png" alt="Tadaa"></a>
          </div>-->
          
          <div class="simg">
            <?php if ( has_category('videos',$post->ID) ):?>
                  <?php the_content(); ?>
              
            <?php else: ?>
              <?php the_post_thumbnail('main-post-large'); ?>
            <?php endif ?>
          </div>
          <div class="container">
            <div class="row">
              <?php if ( !has_category('videos',$post->ID) ):?>
                <div class="col-lg-2 col-sm-12" id="sidebar-left">
                  <div class="post-author" >
                    <span class="userimg"><?php echo get_avatar(get_the_author_meta('ID'), 96); ?></span>
                    <span class="username"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a></span>
                    <span class="user-desc"><?php echo  get_the_author_meta( 'description' )  ?></span>
                  </div>

               
                </div>
              <?php else: ?>

              <?php endif ?>

              <div class="col-lg-8 col-sm-12">
                <?php if ( !has_category('videos',$post->ID) ):?>
                  <div class="stext">
                    <?php the_content(); ?>
                  </div>
                <?php else: ?>

                <?php endif ?>

                <?php if ( !has_category('videos',$post->ID) ):?>
                  <div class="thb-social-footer style1 sharing-counts-on">
                   
                    <div class="thb-social-footer-buttons">
                      <div class="social-button-holder">

                       <a onclick="window.open('https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');"  id="soc" class="social social-facebook">
                          <span class="thb-social-icon">
                            <i class="fab fa-facebook-f"></i>
                            <span class="thb-social-text">Facebook</span>
                          </span>
                        </a>
                      </div>
                      <div class="social-button-holder">
                        <a onclick="window.open('https://twitter.com/share?text=<?php the_title(); ?>&amp;via=tadaa&amp;url=<?php the_permalink(); ?>', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');" id="soc" class="social social-twitter">
                          <span class="thb-social-icon">
                            <i class="fab fa-twitter"></i>
                            <span class="thb-social-text">Tweet</span>
                          </span>
                          </a>
                        </div>
                        <div class="social-button-holder">
                          <a onclick="window.open('https://pinterest.com/pin/create/bookmarklet/?url=<?php the_permalink(); ?>&amp;media=<?php the_title(); ?>', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');"  id="soc" class="social social-pinterest">
                            <span class="thb-social-icon">
                              <i class="fab fa-pinterest-p"></i>
                              <span class="thb-social-text">Pin</span>
                            </span>
                          </a>
                          </div>


                        <div class="social-button-holder">
                          <a onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&summary=', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');"  id="soc" class="social social-linkedin" > 
                            <span class="thb-social-icon">
                              <i class="fab fa-linkedin-in"></i>
                              <span class="thb-social-text">Linkedin</span>
                            </span>
                          </a>
                        </div>
                      </div>
                       <div class="stext-bg">
                        <div class="post-author" id="post-author" style="top: 30px;position: relative;">
                          <span class="userimg">    <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>" id="author-circle"> <?php echo get_avatar(get_the_author_meta('ID'), 96); ?> </a></span>
                              <span class="username"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a></span>
                              <span class="user-desc"><?php echo  get_the_author_meta( 'description' )  ?></span>
                          </div>

                <div class="tags">
                  <span>ტეგები:</span>
                  <?php
                    $before = '';
                    $seperator = ''; // blank instead of comma
                    $after = '';
                    the_tags( $before, $seperator, $after );
                  ?>
                </div>
               
                <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="2"></div>


              
            </div>
                  </div>
              <?php else: ?>

              <?php endif ?>
                
              </div>
              <?php if ( !has_category('videos',$post->ID) ):?>
                <div class="col-lg-2 col-sm-12" id="sidebar">

                  <h3 class="side-title">მსგავსი სტატიები</h3>

                  <div class="row">
                     <?php
                      $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 5, 'orderby' => 'rand', 'post__not_in' => array($post->ID) ) );
                      if( $related ) foreach( $related as $post ) {
                      setup_postdata($post); ?>
                      <div class="col-12 col-md-12">

                        <figure class="post-gallery">
                          <a href="<?php the_permalink(); ?>">
                          <img width="270" height="270" src="<?php echo get_the_post_thumbnail_url()  ?>" class="last-news-image" id="related-posts" alt="" sizes="74px" data-src="" data-sizes="auto" data-srcset="">
                          </a>
                        </figure>
                      </div>
                      <div class="col-6 col-md-12">
                        <div class="last-news-meta">
                          <h2><a href="<?php the_permalink(); ?>" id="related-post-title"><?php the_title(); ?></a></h2>
                          <br>
                        </div>
                      </div>
                      <?php }
                      wp_reset_postdata(); ?>
                  </div>
                </div>
              <?php else: ?>

              <?php endif ?>
            </div>
          </div>


        <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
    <div class="col-1">
    </div>
  </div>
</div>
<?php get_footer(); ?>
