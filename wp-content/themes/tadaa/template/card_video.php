<?php if ($iteration == 1): ?>
	<div class="card video-box">
	   	<img class="card-img-top popular-image" src="<?php echo get_the_post_thumbnail_url()  ?>" alt="tadaa.ge">
	    <div class="overlay">
		  <a href="<?php the_permalink(); ?>" class="icon" title="Play">
			<i class="far fa-play-circle"></i>
		  </a>
		</div>
	    <div class="card-body" id="video-title">
	    	<a href="<?php the_permalink(); ?>" class="popular-box-title"><?php the_title(); ?></a>
	    </div>
	</div>
<?php endif ?>



<?php if ($iteration >= 2): ?>

	<div class="left-video-box">
	  <div class="lefttbox-card">
	    <div class="lefttbox-readtime">
	    	<div class="row">
          <div class="col-12 col-md-2">
            <figure class="post-gallery">
              <a href="<?php the_permalink(); ?>">
              <img width="270" height="270" src="<?php echo get_the_post_thumbnail_url()  ?>" class="video-news-image" alt="" sizes="74px" data-src="" data-sizes="auto" data-srcset="">
              </a>
            </figure>
			<div class="overlaysm">
			  <a href="<?php the_permalink(); ?>" class="iconsm" title="Play">
				<i class="far fa-play-circle"></i>
			  </a>
			</div>
          </div>
          <div class="col-6 col-md-10">
            <div class="video-block-meta">
              <span class="lefttbox-date"><?php echo my_custom_date( get_the_date() ); ?></span>
              <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
            </div>
          </div>
	      </div>
	    </div>
	  </div>
	</div>



<?php endif ?>

