
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="slide-menu-logo"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/imgs/tadaa.svg" alt="Tadaa"></a></div>

  <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>

  <div class="newletter-hamburger">
    <?php echo do_shortcode( '[mc4wp_form id="1696"]' ) ?>
  </div>

</div>


<!--HEADER Start-->
<div class="desctop sticky-top">
  <header class="container-fluid ">
    <div class="mobile ">
    <div class="">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-sm-2 col-md-3 logo">
            <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/imgs/tadaa.svg" class="header-logo" alt="Tadaa"></a>
          </div>
          <div class="col-lg-6 col-sm-6 col-md-6">
            <!--NAV Start-->
            <nav class="navigation">
              <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
            </nav>



            <!--NAV End-->
          </div>
          <div class="col-lg-3 col-sm-4 col-md-3 text-right social">
            <ul>
              <li><a target="_blank" href="https://www.facebook.com/tadaa.ge"><i class="fab fa-facebook-f"></i></a></li>
              <li><a target="_blank" href="https://www.instagram.com/tadaa.ge"><i class="fab fa-instagram"></i></a></li>
              <li><a target="_blank" href="https://www.linkedin.com/company/tadaage"><i class="fab fa-linkedin-in"></i></a></li>
              <li><a target="_blank" href="https://www.youtube.com/channel/UC1MYW3DOj2diEKnw9TlMF8w"><i class="fab fa-youtube"></i></a></li>
              <li><a href="#search"><i class="far fa-search"></i></a></li>

            </ul>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  </header>
</div>
<div class="mobheader nav-top">
  <div class="container">
    <div class="row">
      <div class="mobnav col-2">
        <div class="">
          <span class="hamburger material-icons" id="ham"><i class="fal fa-bars"></i></span>
        </div>
        <nav class="nav-drill">
          <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
          <div class="mobsocial">
            <span class="facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></span>
            <span class="instagram"><a href="#"><i class="fab fa-instagram"></i></a></span>
            <span class="linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a></span>
            <span class="youtube"><a href="#"><i class="fab fa-youtube"></i></a></span>
          </div>
        </nav>
      </div>
      <div class="moblogo col-8">
        <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/imgs/tadaa.svg" alt="Tadaa"></a>
      </div>
      <div class="mobsearch col-2">
        <ul class="nav navbar-nav navbar-right text-center">
          <li><a href="#search"><i class="far fa-search"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
