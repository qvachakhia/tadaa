<!DOCTYPE html>
<html lang="ka">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>TADAA.GE - ციფრული საინფორმაციო პლატფორმა</title>
  <meta name="description" content="ტადა არის ციფრული საინფორმაციო პლატფორმა" />
  <meta name="keywords" content="ტესტ ქივორდი, ტესტ ქივორდი, ტესტ ქივორდი, ტესტ ქივორდი, ტესტ ქივორდი, " />
  <link rel="icon" href="http://tadaa.ge/wp-content/uploads/2019/12/logo_icon.ico" type="image/x-icon"/>

  <!--OG-->
  <meta property="fb:app_id" content="" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php the_title(); ?>" />
  <meta property="og:description" content="<?php echo get_the_excerpt(); ?>" />
  <meta property="og:url" content="<?php the_permalink(); ?>" />
  <meta property="og:image" content="<?php
        $thumb_id = get_post_thumbnail_id();
        $thumb_url = wp_get_attachment_image_src($thumb_id,'main-post', true);
        echo $thumb_url[0];
    ?>" />
  <!--CSS-->
  <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
  <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css" />
  <!--JS-->
  <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery-3.3.1.min.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/assets/js/mine.js"></script>
  <?php wp_head(); ?>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152411751-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-152411751-1');
  </script>
</head>

<body class="homepage">

<?php include( locate_template( 'template/header.php', false, false ) );  ?>
