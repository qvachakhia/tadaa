<!--FOOTER Start-->
<footer>
  

  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-sm-12  col-md-12 fbox" id="footer-menu">
        <div class="footer-menu">
          <ul>
            <!--<li><a href="/">კატეგორია</a></li>-->
            <!--<li><a href="/">კატეგორია</a></li>-->
            <!--<li><a href="/">კატეგორია</a></li>-->
            <!--<li><a href="/">კატეგორია</a></li>-->
            
          </ul>
        </div>
      </div>
      
      <div class="col-lg-4 col-sm-12 col-md-12 fbox" id="soc-t">
        <div class="footer-logo">
          <img src="<?php bloginfo('template_url'); ?>/imgs/tadaa-white.svg" alt="tadaa">
        </div>
      </div>
      <div class="col-lg-4 col-sm-12  col-md-12 fbox">

        <div class="social-footer" id="social-footer">
          <ul>
            <li><a target="_blank" href="https://www.facebook.com/tadaa.ge"><i class="fab fa-facebook-f"></i></a></li>
            <li><a target="_blank" href="https://www.instagram.com/tadaa.ge"><i class="fab fa-instagram"></i></a></li>
            <li><a target="_blank" href="https://www.linkedin.com/company/tadaage"><i class="fab fa-linkedin-in"></i></a></li>
            <li><a target="_blank" href="https://www.youtube.com/channel/UC1MYW3DOj2diEKnw9TlMF8w"><i class="fab fa-youtube"></i></a></li>
          </ul>
        </div>

      </div>
    </div>
  </div>

</footer>
<!--FOOTER End-->
<?php wp_footer();?>

<!--SEARCH FORM-->
<div id="search">
  <button type="button" class="close"></button>
  <div class="s-box">
    <form method="get" id="searchform" action="/">
      <div class="s-input">
        <input type="search" name="s" value="<?php the_search_query(); ?>" placeholder="რას ეძებ?" />
      </div>
      <div class="s-button">
        <button id="searchsubmit" type="submit" class="btn"><i class="far fa-search"></i></button>
      </div>
      <div class="clear"></div>
    </form>
  </div>
</div>

<script>
$(function () {
    $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });
    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
});
</script>

<!--MOBILE NAV JS-->
<script>
console.clear()
const navExpand = [].slice.call(document.querySelectorAll('.nav-expand'))
const backLink = `<li class="nav-item">
	<a class="nav-link nav-back-link" href="javascript:;">
		Back
	</a>
</li>`
navExpand.forEach(item => {
	item.querySelector('.nav-expand-content').insertAdjacentHTML('afterbegin', backLink)
	item.querySelector('.nav-link').addEventListener('click', () => item.classList.add('active'))
	item.querySelector('.nav-back-link').addEventListener('click', () => item.classList.remove('active'))
})
const ham = document.getElementById('ham')
ham.addEventListener('click', function() {
	document.body.classList.toggle('nav-is-toggled')
})
</script>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0"></script>

</body>
</html>
