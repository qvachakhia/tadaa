<style>
.share-buttons {
    font-size: 0.7rem;
    line-height: 0.7rem;
    letter-spacing: 1px;
    text-transform: uppercase;
    margin: 0px 0 0 0;
    z-index: 2;
    position: relative;
    text-align: center;
    list-style-type: none;
    padding: 0;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between;
    align-content: flex-start;
}

.share-buttons li {
    height: auto;
    flex: 0 1 auto;
    width: calc(25% - 1px);
    margin-right: 1px;
}

.share-buttons li:last-child {
    width: 25%;
    margin-right: 0;
}

.share-buttons svg {
    fill: #fff;
    margin-right: 5px;
    width: 16px;
    height: 16px;
}

.share-googleplus svg {
    width: 20px;
    height: 16px;
}

.share-buttons a {
    display: block;
    padding: 12px 12px 9px;
    text-align: center;
}

.share-buttons li:first-child a {
    border-radius: 3px 0 0 3px;
}

.share-buttons li:last-child a {
    border-radius: 0 3px 3px 0;
}

.share-facebook	{ background: #3b5998;}
.share-facebook i	{ color: #fff; font-size: 15px;}
.share-facebook	span { color: #fff;}

.share-linkedin	{ background: #0077B5;}
.share-linkedin i	{ color: #fff; font-size: 15px;}
.share-linkedin	span { color: #fff;}

.share-twitter	{ background: #1da1f2;}
.share-twitter i	{ color: #fff; font-size: 15px;}
.share-twitter	span { color: #fff;}

.share-pinterest	{ background: #b5071a;}
.share-pinterest i	{ color: #fff; font-size: 15px;}
.share-pinterest	span { color: #fff;}

</style>

<ul class="share-buttons">
  <li>
    <a class="share-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo home_url(); ?>" onclick="window.open(this.href, 'mywin',
    'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;">
      <i class="fab fa-facebook-f"></i>
      <span>Share</span>
    </a>
  </li>
  <li>
    <a class="share-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo home_url(); ?> Webpages&summary=chillyfacts.com&source=Chillyfacts" onclick="window.open(this.href, 'mywin',
    'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" >
      <i class="fab fa-linkedin-in"></i>
      <span>Share</span>
    </a>
  </li>
  <li>
      <a class="share-twitter" href="https://twitter.com/intent/tweet?text=<?php echo $title; ?>&amp;url=<?php echo $url; ?>&amp;via=tadaa.ge" onclick="window.open(this.href, 'mywin',
      'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" >
          <i class="fab fa-twitter"></i>
          <span>Tweet</span>
      </a>
  </li>
  <li>
      <a class="share-pinterest" href="http://pinterest.com/pin/create/button/?url=<?php echo $url; ?>&amp;media=<?php echo $media;   ?>&amp;description=<?php echo $title; ?>" onclick="window.open(this.href, 'mywin',
      'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" >
          <i class="fab fa-pinterest"></i>
          <span>Pin</span>
      </a>
  </li>
</ul>
