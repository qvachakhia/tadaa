<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'i6165405_wp1' );

/** MySQL database username */
define( 'DB_USER', 'i6165405_wp1' );

/** MySQL database password */
define( 'DB_PASSWORD', 'C.Pa3Shtjio9W4VOHVs07' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         'alqWZCfaesdVE0Qr3D79KVSOzx66MajXdrfrYq5J3M18p5bo9XmCNJxpu95cBMWR');
define('SECURE_AUTH_KEY',  'c3ctcI1swaH4g3eMHmUZqC2V62mDfPIdPeDAeh5SyFCby076XKIWehqBrmaaLLO8');
define('LOGGED_IN_KEY',    'pB7qkgby4oJTUHkx0bEE5Z7e0QmB83T9bahT8O2UfGuo3lX7m8TCj5XtvW6ht548');
define('NONCE_KEY',        'AflWPpZ5MhjXTtT2fqKZNHn0pIhKp1VdZrTNQcMiso2uhJaU7SHYXQDCzjs0u5hc');
define('AUTH_SALT',        '2WT6eBEIkqnigG8Bjtq6CXtkl92zXUcHqbf78W6lCt3Vx6svpaaZ44be6l99rpvr');
define('SECURE_AUTH_SALT', 'kRThO6e3a1gKSjwtvL27Wp0ZF63b2dc4TjqhcnlNIobef6f59m1FLGt7VFZfIXvP');
define('LOGGED_IN_SALT',   'O2ffu4SfAxOIF6s0YPG8I8v8PHcfk2yqV9iIdtHAicOnwddNpPrSctbqtTdvm8pQ');
define('NONCE_SALT',       'Ew8xJCm8fXfTyIitqxWbldChJAMNXoroHbVGM0cxd7fvaY9D5jOHO6HG4J81C61i');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');
define('FS_CHMOD_DIR',0755);
define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed externally by Installatron.
 * If you remove this define() to re-enable WordPress's automatic background updating
 * then it's advised to disable auto-updating in Installatron.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
